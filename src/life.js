var life;

function Life(height, width){
    this.field = new Array(height).fill(0).map(i => i = new Array(width).fill(0));
    var self = this;
    var canvas = document.getElementById('cField');

    if (canvas) {
        canvas.height = height * 10;
        canvas.width = width * 10;
        canvas.onclick = function(event){
            var x = Math.floor(event.offsetX/10);
            var y = Math.floor(event.offsetY/10);
            self.field[x][y] = 1;
            ctx.fillRect(x * 10, y * 10, 10, 10);
        }
    }

    var ctx = canvas && canvas.getContext('2d');

    this.getState = function(y,x){
        var yTop = y === 0 ? height - 1 : y - 1;
        var yBottom = y === height - 1 ? 0 : y + 1;
        var xLeft = x === 0 ? width - 1 : x - 1; 
        var xRight = x === width - 1 ? 0 : x + 1;
        var countNeightbors = 
            this.field[yTop][xLeft] +
            this.field[yTop][x] +
            this.field[yTop][xRight] +
            this.field[y][xLeft] +
            this.field[y][xRight] +
            this.field[yBottom][xLeft] +
            this.field[yBottom][x] +
            this.field[yBottom][xRight];
        
        return ((this.field[y][x] && countNeightbors === 2) || countNeightbors === 3) ? 1 : 0;
    }

    this.nextGeneration = function(){
        var newField = new Array(height).fill(0).map(i => i = new Array(width).fill(0));
        ctx.clearRect(0, 0, height * 10, width * 10);
        newField.forEach((item, y) => {
            item.forEach((item, x) => { 
                newField[y][x] = this.getState(y,x); 
                newField[y][x] && ctx.fillRect(y * 10, x * 10, 10, 10);
            })
        });
        this.field = newField;
    };

    this.run = function(){
        self.nextGeneration();
        setTimeout(function(){self.run();}, 300);
    }
}

function createLife(){
    var y = Number(document.getElementById('y').value);
    var x = Number(document.getElementById('x').value);
    life = new Life(y,x);
}

function runLife(){
    life.run();
}